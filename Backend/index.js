
let products = [{
        name: 'book',
        category: 'book',
    },
    {
        name: 'potato chips',
        category: 'food',
    },
    {
        name: 'pencil',
        category: 'stationery',
    },
    {
        name: 'shirt',
        category: 'clothing',
    }
];

let locations = [{
        country: 'CA',
        taxRate: 9.75,
        excemptProduct: ['food']
    },
    {
        country: 'NY',
        taxRate: 8.875,
        excemptProduct: ['food', 'clothing']
    }
]

class ShoppingCart {
    constructor(products, locations) {
        this.products = products
        this.locations = locations
        this.receipt = {
            item: [],
            subtotal: null,
            salesTax: null,
            total: null
        }

    }

    caculateSalesTax(price, quantity, taxRate) {
        const salesTax = price * quantity * taxRate / 100
        return (Math.ceil(salesTax * 2 * 10) / 2 / 10)
    }

    renderReceipt(country, invoice) {
        let receipt = {
            item: [],
            subtotal: null,
            salesTax: null,
            total: null
        }

        this.locations.map((location) => {
            if (location.country === country) {
                this.products.map((product) => {
                    invoice.map((item) => {
                        if (product.name === item.name) {
                            if (location.excemptProduct.indexOf(product.category) === -1) {
                                const salesTax = this.caculateSalesTax(item.price, item.quantity, location.taxRate)
                                const newReceipt = item
                                newReceipt.salesTax = salesTax
                                receipt.item.push(newReceipt)
                            } else {
                                const newReceipt = item
                                receipt.item.push(newReceipt)
                            }
                        }
                    })

                })

            }
        })

        receipt.item.map((product) => {
            const subtotal = product.price * product.quantity
            receipt.subtotal = Math.ceil((receipt.subtotal += subtotal) * 100) / 100
            if (product.salesTax) {
                receipt.salesTax += product.salesTax
            }
        })

        receipt.salesTax = Number(receipt.salesTax.toFixed(2))

        receipt.total = Math.ceil((receipt.subtotal + receipt.salesTax) * 100) / 100

        return receipt

    }

}

const newCart = new ShoppingCart(products, locations)
// Input: Location: CA, 1 book at 17.99, 1 potato chips at 3.99
// Input: Location: NY, 1 book at 17.99, 3 pencils at 2.99
// Input: Location: NY, 2 pencils at 2.99, 1 shirt at 29.99
const case1 = [{
        name: 'book',
        price: 17.99,
        quantity: 1,
    },
    {
        name: 'potato chips',
        price: 3.99,
        quantity: 1
    }
]
const case2 = [{
    name: 'book',
    price: 17.99,
    quantity: 1
}, {
    name: 'pencil',
    price: 2.99,
    quantity: 3
}]
const case3 = [{
    name: 'pencil',
    price: 2.99,
    quantity: 2
}, {
    name: 'shirt',
    price: 29.99,
    quantity: 1
}]
const case4 = case1.concat(case2, case3)
console.log(newCart.renderReceipt('CA', case1))
console.log(newCart.renderReceipt('NY', case2))
console.log(newCart.renderReceipt('NY', case3))
console.log(newCart.renderReceipt('CA', case4))
console.log(newCart.renderReceipt('NY', case4))