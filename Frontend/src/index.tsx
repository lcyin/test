import React from "react";
import ReactDOM from "react-dom";
import Switch from "./Switch";
import './index.css'

interface State {
    isSwitchChecked: boolean;
    isSwitchDisabled: boolean;
}

class App extends React.PureComponent<{}, State> {
    public state: State = {
        isSwitchChecked: false,
        isSwitchDisabled: false,
    };

    onToggleDisable = () => this.setState({ isSwitchDisabled: !this.state.isSwitchDisabled });

    onChange = (checked: boolean) => this.setState({ isSwitchChecked: checked });

    render() {
        return (
            <div>
                <Switch checked={this.state.isSwitchChecked} disabled={this.state.isSwitchDisabled} onChange={this.onChange} />
                <div className='disableDiv'>
                    <h1 className='disableText'>Disable </h1>
                    <button type="button" className={this.state.isSwitchDisabled?'trueToggle':'falseToggle'} onClick={this.onToggleDisable}>
                        {this.state.isSwitchDisabled ? 'True' : 'False'}
                    </button>
                </div>

            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById("app")!);
