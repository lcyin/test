import React, { ChangeEvent } from "react";
import "./index.css";

// If you wanna implement by TypeScript (Advanced Requirement 1), then rename this file to index.tsx and remove index.jsx
// Otherwise, remove this file

interface Props {
    checked: boolean;
    onChange: (checked: boolean) => void;
    disabled?: boolean;
}

export default class Switch extends React.PureComponent<Props> {
    constructor(props: Props) {
        super(props);
    }

    private handleTofuChange = (e: ChangeEvent<HTMLInputElement>) => {
        console.log(e.target.checked)
        if (!this.props.disabled) {
            this.props.onChange(e.target.checked)
        } else {
            this.props.onChange(false)
        }
    }
    
    render() {
        return (
            <div className="comp-switch">
                <div className='switchContainer'>
                    <label >
                        <input type="checkbox"
                            checked={this.props.disabled ? false : this.props.checked}
                            onChange={this.handleTofuChange}
                            className='switch'
                            ref='switch' />
                        <div>
                            <div></div>
                        </div>
                    </label>
                </div>
            </div>);
    }
}
